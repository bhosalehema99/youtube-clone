const videocontainercard = document.querySelector('.video-container');
// dsfads
let api_key ="AIzaSyDpuk1Wvvax8YnxNCP-iXxD1i600QxxnNc";
let video_http = "https://www.googleapis.com/youtube/v3/videos?";
let channel_http = "https://www.googleapis.com/youtube/v3/channels?";
fetch(video_http + new URLSearchParams({
    key : api_key,
    part:'snippet',
    chart:'mostPopular',
    maxResults:500,
    regionCode:'IN'
}))
.then(res => res.json())
.then(data => {
//console.log(data);
 data.items.forEach(item =>{
    getChannelIcon(item);
 })   
})
.catch(err =>console.log(err));
 const getChannelIcon =(video_data)=>{
    fetch(channel_http + new URLSearchParams({
        key : api_key,
        part:'snippet',
        id:video_data.snippet.channelId   
    }))
    .then(res => res.json())
    .then(data =>{
  video_data.channelThumbnail = data.items[0].snippet.thumbnails.default.url;
  makeVideoCard(video_data);        
    })
 }
const makeVideoCard = (data) =>{
    videocontainercard.innerHTML += `
    <div class="video" onclick="location.href='https://youtube.com/watch?v=${data.id}'">
             <img src="${data.snippet.thumbnails.high.url}" alt="thumbnail" class="thumbnail">
             <div class="content">
                 <img src="${data.channelThumbnail}" alt="channel-icon" class="channel-icon">
                 <div class="info">
                     <h4 class="title">${data.snippet.title}</h4>
                     <p class="channel-name">${data.snippet.channelTitle}</p>
                 </div>
             </div>
         </div>
    
    `;
}
const searchbar = document.querySelector('.search-bar');
const searchbtn = document.querySelector('.search-btn');
let searchlink = "https://www.youtube.com/results?search_query=";

searchbtn.addEventListener('click', () =>{
    if(searchbar.value.length){
        location.href=searchlink+searchbar.value;

    }
})




































































































